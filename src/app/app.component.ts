import { Component, OnInit } from '@angular/core';
import { VideoService } from './video.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  videoUri: string;

  question = 'What are the lights shown on earth\'s atmosphere called?';

  opts = [
    { title: 'Aurora', value: 'aurora' },
    { title: 'Arora', value: 'arora' },
    { title: 'Chemical Reaction', value: 'chemReaction' }
  ];

  title = 'video-question';

  videoEnded = false;

  constructor(private videoService: VideoService) {}

  ngOnInit() {
    this.videoUri = this.videoService.videoUrl();
  }

  btnClick(value: string) {
    // alert(JSON.stringify({ value }));
    alert( JSON.stringify({value}) + '\nAnswer is ' + this.videoService.videoAnswer(value));
  }

}
