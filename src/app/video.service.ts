import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VideoService {

  constructor() { }

  videoUrl() {
    return 'http://static.videogular.com/assets/videos/videogular.mp4';
  }

  videoAnswer(answer: string) {
    if (answer === 'aurora') {
      return true;
    } else {
      return false;
    }
  }
}
